import React from 'react';
import ReactDOM from 'react-dom';
import GlobalWeather from './components/GlobalWeather';

import mapboxgl from "mapbox-gl";
mapboxgl.accessToken = 'pk.eyJ1Ijoia2FjcGVyeiIsImEiOiJja2Z4b29qaWEwMms2MnFudjRma2k2Z3liIn0.n9M08OnwsB3859p40G30Xg';

ReactDOM.render(
  <React.StrictMode>
    <GlobalWeather />
  </React.StrictMode>,
  document.getElementById('root')
);
