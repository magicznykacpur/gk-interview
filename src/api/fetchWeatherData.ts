export default async (coordinates: { lng: number; lat: number }): Promise<any> =>
  fetch(
    `https://api.openweathermap.org/data/2.5/weather?lat=${coordinates.lat}&lon=${coordinates.lng}&appid=5d897581477dd4febad056d3173eb026&units=metric`
  ).then(
    (weather) => {
      return weather.json();
    },
    (error) => {
      return error;
    }
  );
