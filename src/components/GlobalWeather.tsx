import React, { useEffect, useLayoutEffect, useState } from 'react';
import Countries from './Countries';
import styled from 'styled-components';
import mapboxgl from 'mapbox-gl';
import fetchWeatherData from '../api/fetchWeatherData';
import useLocalStorage from 'react-use-localstorage';
import {initTemperatureMarker} from "../utils/mapUtils";

const GlobalWeather = () => {
  const [latestMarker, setLatestMarker] = useLocalStorage('latestMarker');
  const [map, setMap] = useState<mapboxgl.Map>();
  const [, setCurrentMarker] = useState<mapboxgl.Marker>();

  const flyTo = (coordinates: { lng: number; lat: number }) => {
    map?.flyTo({
      center: coordinates,
      essential: true,
    });

    pinLocation(coordinates);
    setLatestMarker(JSON.stringify(coordinates));
  };

  const pinLocation = async ({ lng, lat }: { lng: number; lat: number }) => {
    const weatherData = await fetchWeatherData({ lng, lat });
    const marker = initTemperatureMarker(lng, lat, weatherData, map!)

    setCurrentMarker(marker);
  };

  useLayoutEffect(() => {
    const mapInstance = new mapboxgl.Map({
      container: 'map-container',
      style: 'mapbox://styles/mapbox/streets-v11',
      zoom: 5,
    });

    setMap(mapInstance);
  }, []);

  useEffect(() => {
    if (map) {
      flyTo(JSON.parse(latestMarker || ''));
    }
    // eslint-disable-next-line
  }, [map]);

  return (
    <GlobalWeatherContainer>
      <Countries flyTo={flyTo} />
      <MapContainer id="map-container" />
    </GlobalWeatherContainer>
  );
};

const GlobalWeatherContainer = styled.div`
  display: flex;
`;

const MapContainer = styled.div`
  width: 100vw;
  height: 100vh;
`;

export default GlobalWeather;
