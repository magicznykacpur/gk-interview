import React from 'react';
import styled from 'styled-components';

const Countries = ({ flyTo }: { flyTo: (coordinates: { lng: number; lat: number }) => void }) => {
  const countryJson = require('../assetes/countries.json');

  const countryList = countryJson.map(({ name, latlng }: { name: string; latlng: string[] }) => (
    <Country key={latlng.join('.')} onClick={() => flyTo({ lng: Number(latlng[1]), lat: Number(latlng[0]) })}>
      {name}
    </Country>
  ));

  return <CountryList>{countryList}</CountryList>;
};

const CountryList = styled.ol`
  position: fixed;
  left: 0;
  z-index: 10;

  width: 25%;
  max-height: 90vh;
  padding-left: 1rem;
  margin-top: 0;

  background-color: white;

  list-style: none;
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.75);

  overflow-y: auto;
`;

const Country = styled.li`
  position: relative;
  top: 0;

  max-width: 80%;
  padding: 0.5rem;

  :hover {
    top: -0.25rem;
    cursor: pointer;
    font-weight: 600;
  }
`;

export default Countries;
