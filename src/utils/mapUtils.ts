import mapboxgl from 'mapbox-gl';

export const initTemperatureMarker = (lng: number, lat: number, weatherData: any, map: mapboxgl.Map) => {
  const {
    main: { temp, temp_max, temp_min, feels_like, humidity },
  } = weatherData;

  return new mapboxgl.Marker()
    .setLngLat({ lng, lat })
    .addTo(map)
    .setPopup(
      new mapboxgl.Popup().setHTML(
        `Feels like: <strong>${feels_like} °C</strong><br/>
            Temp: <strong>${temp} °C</strong><br/>
            Max temp: <strong>${temp_max} °C</strong><br/>
            Min temp: <strong>${temp_min} °C</strong><br/>
            Humidity: <strong> ${humidity} %</strong>`
      )
    );
};
